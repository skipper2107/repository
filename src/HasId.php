<?php
namespace Skipper\Repository;

use Skipper\Repository\Contracts\Entity;

trait HasId
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Entity
     */
    public function setId(int $id): Entity
    {
        $this->id = $id;

        return $this;
    }
}