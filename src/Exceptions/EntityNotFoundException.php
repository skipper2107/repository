<?php
namespace Skipper\Repository\Exceptions;

use Skipper\Exceptions\Error;
use Throwable;

class EntityNotFoundException extends StorageException
{
    public function __construct(
        string $location,
        array $context = [],
        Throwable $previous = null,
        int $code = 0
    ) {
        parent::__construct('Entity not found', $location, $context, $previous, $code);

        $this->errors = [];
        $this->addError(new Error('Entity not found', 'notFound', $location));
    }
}