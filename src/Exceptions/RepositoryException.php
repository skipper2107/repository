<?php
namespace Skipper\Repository\Exceptions;

use Skipper\Exceptions\DomainException;
use Skipper\Exceptions\Error;
use Throwable;

class RepositoryException extends DomainException
{
    public function __construct(
        string $message,
        string $location,
        array $context = [],
        Throwable $previous = null,
        int $code = 0
    ) {
        parent::__construct($message, $location, $context, $previous, $code);

        $this->errors = [];
        $this->addError(new Error($message, 'storageError', $location));
    }
}