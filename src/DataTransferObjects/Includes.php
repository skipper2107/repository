<?php
namespace Skipper\Repository\DataTransferObjects;

final class Includes
{
    /**
     * @var string
     */
    private $entityName;

    /**
     * @var Filter[]
     */
    private $filters;

    public function __construct(string $entityName, array $filters = [])
    {
        $this->entityName = $entityName;
        $this->filters = $filters;
    }

    /**
     * @return string
     */
    public function getEntityName(): string
    {
        return $this->entityName;
    }

    /**
     * @return Filter[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }
}