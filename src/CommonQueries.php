<?php
namespace Skipper\Repository;

use Skipper\Repository\Contracts\Entity;
use Skipper\Repository\Exceptions\EntityNotFoundException;

trait CommonQueries
{
    /**
     * @param int $id
     * @return Entity
     * @throws EntityNotFoundException
     */
    public function find(int $id): Entity
    {
        return $this->findOneBy([
            'filter' => [
                'id' => ['value' => $id],
            ],
        ]);
    }

    /**
     * @param array $criteria
     * @throws EntityNotFoundException
     * @return Entity
     */
    public function findOneBy(array $criteria): Entity
    {
        $criteria['pagination']['limit'] = 1;
        $found = $this->findAll($criteria);
        if (empty($found)) {
            throw new EntityNotFoundException(get_class($this), [
                'criteria' => $criteria,
            ]);
        }

        return reset($found);
    }

    /**
     * @param int[] $ids
     * @return Entity[]
     */
    public function getAllByIds(array $ids): array
    {
        return $this->findAll([
            'filter' => [
                'id' => [
                    'operator' => 'in',
                    'value' => $ids,
                ],
            ],
        ]);
    }
}