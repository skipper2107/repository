<?php
namespace Skipper\Repository;

use Skipper\Repository\Contracts\Repository;
use Skipper\Repository\DataTransferObjects\Filter;
use Skipper\Repository\DataTransferObjects\Includes;
use Skipper\Repository\DataTransferObjects\Pagination;

abstract class CriteriaAwareRepository implements Repository
{
    use CommonQueries;

    public const FILTER = 'filter';
    public const VALUE = 'value';
    public const PAGINATION = 'pagination';
    public const LIMIT = 'limit';
    public const OFFSET = 'offset';
    public const OPERATOR = 'operator';
    public const INVERSE = 'inverse';
    public const SORT = 'sort';
    public const JOIN = 'join';

    /**
     * @param array $criteria
     * @return Pagination|null
     */
    public function getPaginationFromCriteria(array $criteria): ?Pagination
    {
        if (array_key_exists(self::PAGINATION, $criteria)) {
            return null;
        }

        return new Pagination(
            $criteria[self::PAGINATION][self::OFFSET] ?? 0,
            $criteria[self::PAGINATION][self::LIMIT] ?? 20
        );
    }

    /**
     * @param array $criteria
     * @return Filter[]
     */
    protected function getFiltersFromCriteria(array $criteria): array
    {
        foreach ($criteria[self::FILTER] ?? [] as $column => $value) {
            $filters[] = new Filter(
                $column,
                $value[self::VALUE] ?? null,
                $this->getOperator($value, $value[self::INVERSE] ?? false)
            );
        }

        return $filters ?? [];
    }

    /**
     * @param array $criteria
     * @param bool $isInverse
     * @return string
     */
    private function getOperator(array $criteria, bool $isInverse): string
    {
        $operator = strtolower($criteria[self::OPERATOR] ?? '=');
        switch ($operator) {
            case '=':
            case '==':
            case '===':
                $normalized = '=';
                break;
            case '!=':
            case '!==':
            case '<>':
                $normalized = '!=';
                break;
            case '!like':
            case 'not like':
                $normalized = '!like';
                break;
            case '!in':
            case 'not in':
                $normalized = '!in';
                break;
            default:
                $normalized = in_array($operator, [
                    '>',
                    '<',
                    '>=',
                    '<=',
                    'like',
                    'in',
                ]) ? $operator : '=';
        }

        if (false === $isInverse) {
            return $normalized;
        }

        $inverseMap = [
            '=' => '!=',
            'like' => '!like',
            'in' => '!in',
            '>' => '<=',
            '<' => '>=',
        ];

        if (array_key_exists($normalized, $inverseMap)) {
            return $inverseMap[$normalized];
        }
        $key = array_search($normalized, $inverseMap);
        if (false === $key) {
            return '!=';
        }

        return $key;
    }

    /**
     * @param array $criteria
     * @return array
     */
    protected function getSortsFromCriteria(array $criteria): array
    {
        foreach ($criteria[self::SORT] ?? [] as $column => &$sort) {
            $sort = in_array(strtolower($sort), ['asc', 'desc']) ? strtolower($sort) : 'desc';
        }

        return $criteria[self::SORT] ?? [];
    }

    /**
     * @param array $criteria
     * @return Includes[]
     */
    protected function getIncludesFromCriteria(array $criteria): array
    {
        foreach ($criteria[self::JOIN] ?? [] as $entity => $subCriteria) {
            $joins[] = new Includes($entity, $this->getFiltersFromCriteria($subCriteria));
        }

        return $joins ?? [];
    }
}