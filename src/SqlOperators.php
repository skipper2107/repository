<?php
namespace Skipper\Repository;

trait SqlOperators
{
    /**
     * @param string $commonOperator
     * @return string
     */
    protected function getSqlOperator(string $commonOperator): string
    {
        switch ($commonOperator) {
            case '!=':
                return '<>';
            case '!in':
                return 'not in';
            case '!like':
                return 'not like';
        }

        return $commonOperator;
    }
}