<?php
namespace Skipper\Repository\Contracts;

use Skipper\Repository\Exceptions\EntityNotFoundException;
use Skipper\Repository\Exceptions\StorageException;

/**
 * Interface Repository
 * @package Skipper\Repository
 * @example ['filter' => ['id' => ['in' => [1,2,3], '<>' => 4]], 'sort' => ['id' => 'asc']]
 */
interface Repository
{
    /**
     * @param Entity $entity
     * @throws StorageException
     * @return bool
     */
    public function save(Entity $entity): bool;

    /**
     * @param Entity $entity
     * @return bool
     * @throws StorageException
     */
    public function delete(Entity $entity): bool;

    /**
     * @param int $id
     * @return Entity
     * @throws EntityNotFoundException
     */
    public function find(int $id): Entity;

    /**
     * @param array $criteria
     * @throws EntityNotFoundException
     * @return Entity
     */
    public function findOneBy(array $criteria): Entity;

    /**
     * @param array $criteria
     * @return Entity[]
     */
    public function findAll(array $criteria): array;

    /**
     * @param int[] $ids
     * @return Entity[]
     */
    public function getAllByIds(array $ids): array;

    /**
     * @param array $criteria
     * @return array
     * ['data' => $data, 'total' => $count] = $repo->getAllWithTotalCount([]);
     */
    public function getAllWithTotalCount(array $criteria): array;

    /**
     * @param array $criteria
     * @return int
     */
    public function count(array $criteria): int;

    /**
     * @param array $criteria
     * @return bool
     */
    public function exists(array $criteria): bool;
}