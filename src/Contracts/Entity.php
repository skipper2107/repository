<?php
namespace Skipper\Repository\Contracts;

interface Entity
{
    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @param int $id
     * @return Entity
     */
    public function setId(int $id): Entity;
}